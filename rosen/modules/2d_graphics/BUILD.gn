# Copyright (c) 2022-2023 Huawei Device Co., Ltd.. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/arkui/ace_engine/ace_config.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")
import("$graphic_2d_root/rosen/modules/2d_engine/rosen_text/config.gni")

drawing_root = "$rosen_root/modules/2d_graphics"
drawing_core_include_dir = "$rosen_root/modules/2d_graphics/include"
drawing_core_src_dir = "$rosen_root/modules/2d_graphics/src/drawing"
src_dir = "$rosen_root/modules/2d_graphics/src"
platform_dir = "$rosen_root/modules/platform"

config("2d_graphics_config") {
  include_dirs = [
    "$drawing_core_include_dir",
    "$src_dir",
    "$drawing_core_src_dir",
    "$drawing_core_src_dir/engine_adapter",
    "$platform_dir/image_native",
    "$rosen_root/modules/render_service_base/include",
    "$rosen_root/modules/texgine/texgine_drawing/src",
  ]
}

config("export_config") {
  include_dirs = [
    "$drawing_core_include_dir",
    "$src_dir",
    "$drawing_core_src_dir",
    "$drawing_core_src_dir/engine_adapter",
    "$graphic_2d_root/utils/color_manager/export",
    "$rosen_root/modules/render_service_base/include",
    "$rosen_root/modules/texgine/texgine_drawing/src",
    "$rosen_root/modules/2d_engine/rosen_text/export",
  ]

  if (ace_enable_gpu) {
    defines = [ "ACE_ENABLE_GPU" ]
  }

  if (graphic_2d_feature_enable_opinc) {
    defines += [ "DDGR_ENABLE_FEATURE_OPINC" ]
  }
}

if (enable_text_gine) {
  defines = [ "USE_GRAPHIC_TEXT_GINE" ]
}

template("graphics2d_source_set") {
  forward_variables_from(invoker, "*")

  ohos_source_set(target_name) {
    defines += invoker.defines
    cflags_cc += invoker.cflags_cc
    if (use_texgine) {
      defines += [ "USE_TEXGINE" ]
    } else if (use_skia_txt) {
      defines += [ "USE_SKIA_TXT" ]
    }

    public_deps = []
    external_deps = [ "napi:ace_napi" ]
    sources = [
      "$drawing_core_src_dir/draw/OpListHandle.cpp",
      "$drawing_core_src_dir/draw/brush.cpp",
      "$drawing_core_src_dir/draw/color.cpp",
      "$drawing_core_src_dir/draw/core_canvas.cpp",
      "$drawing_core_src_dir/draw/paint.cpp",
      "$drawing_core_src_dir/draw/path.cpp",
      "$drawing_core_src_dir/draw/pen.cpp",
      "$drawing_core_src_dir/draw/surface.cpp",
      "$drawing_core_src_dir/effect/blender.cpp",
      "$drawing_core_src_dir/effect/blur_draw_looper.cpp",
      "$drawing_core_src_dir/effect/color_filter.cpp",
      "$drawing_core_src_dir/effect/color_matrix.cpp",
      "$drawing_core_src_dir/effect/color_space.cpp",
      "$drawing_core_src_dir/effect/filter.cpp",
      "$drawing_core_src_dir/effect/image_filter.cpp",
      "$drawing_core_src_dir/effect/mask_filter.cpp",
      "$drawing_core_src_dir/effect/path_effect.cpp",
      "$drawing_core_src_dir/effect/runtime_blender_builder.cpp",
      "$drawing_core_src_dir/effect/runtime_effect.cpp",
      "$drawing_core_src_dir/effect/runtime_shader_builder.cpp",
      "$drawing_core_src_dir/effect/shader_effect.cpp",
      "$drawing_core_src_dir/engine_adapter/impl_factory.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/rs_skia_memory_tracer.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_bitmap.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_blender.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_camera.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_canvas.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_canvas_autocache.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_color_filter.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_color_space.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_data.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_font.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_font_mgr.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_font_style_set.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_graphics.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_helper.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_hm_symbol.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_hm_symbol_config_ohos.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_image.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_image_filter.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_impl_factory.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_mask_filter.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_matrix.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_matrix44.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_memory_stream.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_paint.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_path.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_path_effect.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_picture.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_pixmap.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_region.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_runtime_blender_builder.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_runtime_effect.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_runtime_shader_builder.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_shader_effect.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_static_factory.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_surface.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_text_blob.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_text_blob_builder.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_texture_info.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_trace_memory_dump.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_typeface.cpp",
      "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_vertices.cpp",
      "$drawing_core_src_dir/engine_adapter/static_factory.cpp",
      "$drawing_core_src_dir/image/bitmap.cpp",
      "$drawing_core_src_dir/image/image.cpp",
      "$drawing_core_src_dir/image/picture.cpp",
      "$drawing_core_src_dir/image/pixmap.cpp",
      "$drawing_core_src_dir/image/trace_memory_dump.cpp",
      "$drawing_core_src_dir/recording/cmd_list.cpp",
      "$drawing_core_src_dir/recording/cmd_list_helper.cpp",
      "$drawing_core_src_dir/recording/draw_cmd.cpp",
      "$drawing_core_src_dir/recording/draw_cmd_list.cpp",
      "$drawing_core_src_dir/recording/mask_cmd_list.cpp",
      "$drawing_core_src_dir/recording/mem_allocator.cpp",
      "$drawing_core_src_dir/recording/recording_canvas.cpp",
      "$drawing_core_src_dir/text/font.cpp",
      "$drawing_core_src_dir/text/font_mgr.cpp",
      "$drawing_core_src_dir/text/font_style_set.cpp",
      "$drawing_core_src_dir/text/hm_symbol.cpp",
      "$drawing_core_src_dir/text/hm_symbol_config_ohos.cpp",
      "$drawing_core_src_dir/text/text.cpp",
      "$drawing_core_src_dir/text/text_blob.cpp",
      "$drawing_core_src_dir/text/text_blob_builder.cpp",
      "$drawing_core_src_dir/text/typeface.cpp",
      "$drawing_core_src_dir/utils/camera3d.cpp",
      "$drawing_core_src_dir/utils/data.cpp",
      "$drawing_core_src_dir/utils/matrix.cpp",
      "$drawing_core_src_dir/utils/matrix44.cpp",
      "$drawing_core_src_dir/utils/memory_stream.cpp",
      "$drawing_core_src_dir/utils/object_mgr.cpp",
      "$drawing_core_src_dir/utils/region.cpp",
      "$drawing_core_src_dir/utils/resource_holder.cpp",
      "$drawing_core_src_dir/utils/round_rect.cpp",
      "$drawing_core_src_dir/utils/vertices.cpp",
    ]

    if (platform == "ohos" || platform == "ohos_ng") {
      external_deps += [ "init:libbegetutil" ]
      sources += [ "$drawing_core_src_dir/utils/system_properties.cpp" ]
      if (defined(
          graphic_2d_platform_configs.system_graphic_properties_sources)) {
        sources += graphic_2d_platform_configs.system_graphic_properties_sources
      } else {
        sources +=
            [ "$drawing_core_src_dir/utils/system_graphic_properties.cpp" ]
      }
    }

    if (is_arkui_x) {
      defines += [ "CROSS_PLATFORM" ]

      #not needed for arkuix on current version,may need next version
      sources -= [ "$drawing_core_src_dir/engine_adapter/skia_adapter/rs_skia_memory_tracer.cpp" ]
    }
    if (ace_enable_gpu) {
      sources += [
        "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_gpu_context.cpp",
        "$drawing_core_src_dir/image/gpu_context.cpp",
      ]

      defines += [ "ACE_ENABLE_GPU" ]
    }
    if (is_emulator) {
      defines += [ "ROSEN_EMULATOR" ]
    }

    if (graphic_2d_feature_enable_opinc) {
      defines += [ "DDGR_ENABLE_FEATURE_OPINC" ]
    }

    include_dirs = [
      "$drawing_core_include_dir",
      "$src_dir",
      "$drawing_core_src_dir",
      "$drawing_core_src_dir/engine_adapter",
      "$ace_root/frameworks",
      "$rosen_root/modules/render_service_base/include",
      "//third_party/skia/include/core",
      "//third_party/skia/src/core",
      "$rosen_root/modules/texgine/src",
      "$rosen_root/modules/texgine/texgine_drawing/src",
      "$rosen_root/../interfaces/kits/napi/graphic/drawing/common_napi",
    ]

    external_deps += [ "jsoncpp:jsoncpp" ]

    defines += [ "USE_ACE_SKIA" ]

    if (defined(graphic_2d_feature_enable_ddgr) &&
        graphic_2d_feature_enable_ddgr) {
      defines += [ "ENABLE_DDGR_OPTIMIZE" ]
      sources += ddgr_adapter_opt_set.drawing_ddgr_adapter_src_set
      include_dirs += ddgr_adapter_opt_set.drawing_ddgr_adapter_include_set
      public_deps += [ "$graphic_2d_ext_root/ddgr:libddgr" ]
    }

    if (enable_text_gine) {
      defines += [ "USE_GRAPHIC_TEXT_GINE" ]
      if (use_texgine) {
        defines += [ "USE_TEXGINE" ]
      }
      include_dirs += [
        "$flutter_root/engine",
        "$rosen_root/modules/2d_engine/rosen_text",
        "$rosen_root/modules/2d_engine/rosen_text/adapter",
        "$rosen_root/modules/2d_engine/rosen_text/export",
        "$platform_dir/image_native",
      ]
      if (defined(use_new_skia) && use_new_skia) {
        defines += [ "NEW_SKIA" ]
      }
    } else {
      sources += [
        "$rosen_text_root/properties/font_collection_txt.cpp",
        "$rosen_text_root/properties/placeholder_run.cpp",
        "$rosen_text_root/properties/rosen_converter_txt.cpp",
        "$rosen_text_root/properties/text_style.cpp",
        "$rosen_text_root/properties/typography_create_txt.cpp",
        "$rosen_text_root/properties/typography_style.cpp",
        "$rosen_text_root/properties/typography_txt.cpp",
        "$rosen_text_root/ui/font_collection.cpp",
        "$rosen_text_root/ui/typography.cpp",
        "$rosen_text_root/ui/typography_create.cpp",
      ]
      include_dirs += [
        "$ace_napi_frameworks/frameworks",
        "$rosen_2d_engine_root",
      ]
    }
    if (graphic_2d_feature_enable_vulkan) {
      defines += [ "RS_ENABLE_VK" ]
      include_dirs += [ "//third_party/vulkan-headers/include" ]
      public_deps += [ "//third_party/vulkan-headers:vulkan_headers" ]
    }
    if (platform == "ohos" || platform == "ohos_ng") {
      external_deps += [
        "bounds_checking_function:libsec_static",
        "hilog:libhilog",
        "hitrace:hitrace_meter",
      ]
      deps = [ "//third_party/skia:skia_$platform" ]
      defines += [ "USE_GRAPHIC_TEXT_GINE" ]
      deps += [ "$graphic_2d_root/rosen/build/icu:rosen_libicu_$platform" ]
      include_dirs += [ "$flutter_root/engine" ]
      defines += [
        "SUPPORT_OHOS_PIXMAP",
        "ROSEN_OHOS",
      ]

      if (enable_text_gine && use_texgine) {
        include_dirs -= [ "$flutter_root/engine" ]
      }

      if (defined(use_new_skia) && use_new_skia) {
        remove_configs = [ "//build/config/compiler:default_include_dirs" ]
        include_dirs += [
          "$flutter_root/txt/src",
          "//",
          root_gen_dir,
        ]
      } else {
        include_dirs += [ "$flutter_root/engine/flutter/third_party/txt/src" ]
      }
    } else {
      deps = []
      cflags = [ "-std=c++17" ]

      if (!is_arkui_x) {
        deps += [ "$rosen_root/modules/platform:hilog" ]
      }

      external_deps += [ "bounds_checking_function:libsec_static" ]
      deps += [ "$graphic_2d_root/utils:sandbox_utils" ]
      defines += [ "MODULE_DRAWING" ]

      deps += [ "//third_party/skia:skia_$platform" ]
      defines += [ "USE_GRAPHIC_TEXT_GINE" ]
    }
    if (enable_text_gine) {
      deps += [ "$rosen_root/modules/2d_engine/rosen_text:rosen_text_inner" ]
      defines += [ "USE_GRAPHIC_TEXT_GINE" ]
      if (use_skia_txt) {
        include_dirs +=
            [ "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/skia_txt" ]
      }
    }
    if (rosen_is_ohos) {
      defines += gpu_defines
      if (rs_enable_gpu) {
        defines += [
          "GL_GLEXT_PROTOTYPES",
          "EGL_EGLEXT_PROTOTYPES",
        ]
      }
      public_deps += [ "$graphic_2d_root:libgl" ]
      external_deps += [ "c_utils:utils" ]
      public_external_deps = [ "graphic_surface:surface" ]
    }

    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}

foreach(item, ace_platforms) {
  graphics2d_source_set("2d_graphics_source_" + item.name) {
    platform = item.name
    defines = []

    if (defined(item.config)) {
      config = item.config
    } else {
      config = {
      }
    }

    if (defined(config.defines)) {
      defines += config.defines
    }

    if (defined(config.cflags_cc)) {
      cflags_cc = config.cflags_cc
    } else {
      cflags_cc = []
    }
  }
}

if (!is_arkui_x) {
  ## Build 2d_graphics.so
  ohos_shared_library("2d_graphics") {
    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }
    deps = [ ":2d_graphics_source_$platform" ]
    external_deps = [ "hilog:libhilog" ]
    public_configs = [ ":export_config" ]

    if (platform == "ohos") {
      version_script = "2d_graphics.versionscript"
    }

    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}

if (enable_text_gine) {
  ohos_shared_library("2d_graphics_new") {
    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }
    deps = [
      ":2d_graphics_source_$platform",
      "$rosen_root/modules/texgine/texgine_drawing:libtexgine_drawing",
    ]
    external_deps = [ "hilog:libhilog" ]
    public_external_deps = [ "bounds_checking_function:libsec_static" ]
    defines += [ "USE_GRAPHIC_TEXT_GINE" ]
    public_configs = [ ":2d_graphics_config" ]
    innerapi_tags = [ "platformsdk_indirect" ]
    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}

## Build 2d_graphics.so
ohos_shared_library("2d_graphics_canvaskit0310") {
  sources = [
    "$drawing_core_src_dir/draw/brush.cpp",
    "$drawing_core_src_dir/draw/color.cpp",
    "$drawing_core_src_dir/draw/core_canvas.cpp",
    "$drawing_core_src_dir/draw/path.cpp",
    "$drawing_core_src_dir/draw/pen.cpp",
    "$drawing_core_src_dir/effect/blur_draw_looper.cpp",
    "$drawing_core_src_dir/effect/color_filter.cpp",
    "$drawing_core_src_dir/effect/color_space.cpp",
    "$drawing_core_src_dir/effect/filter.cpp",
    "$drawing_core_src_dir/effect/image_filter.cpp",
    "$drawing_core_src_dir/effect/mask_filter.cpp",
    "$drawing_core_src_dir/effect/path_effect.cpp",
    "$drawing_core_src_dir/effect/runtime_effect.cpp",
    "$drawing_core_src_dir/effect/runtime_shader_builder.cpp",
    "$drawing_core_src_dir/effect/shader_effect.cpp",
    "$drawing_core_src_dir/engine_adapter/impl_factory.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_bitmap.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_camera.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_canvas.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_color_filter.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_color_space.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_graphics.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_helper.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_image.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_image_filter.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_impl_factory.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_mask_filter.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_matrix.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_paint.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_path.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_path_effect.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_picture.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_pixmap.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_runtime_effect.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_runtime_shader_builder.cpp",
    "$drawing_core_src_dir/engine_adapter/skia_adapter/skia_shader_effect.cpp",
    "$drawing_core_src_dir/image/bitmap.cpp",
    "$drawing_core_src_dir/image/image.cpp",
    "$drawing_core_src_dir/image/picture.cpp",
    "$drawing_core_src_dir/image/pixmap.cpp",
    "$drawing_core_src_dir/text/text.cpp",
    "$drawing_core_src_dir/utils/camera3d.cpp",
    "$drawing_core_src_dir/utils/matrix.cpp",
  ]

  if (enable_text_gine) {
    defines += [ "USE_CANVASKIT0310_SKIA" ]
  } else {
    defines = [ "USE_CANVASKIT0310_SKIA" ]
  }

  include_dirs = [
    "$drawing_core_include_dir",
    "$src_dir",
    "$drawing_core_src_dir",
    "$drawing_core_src_dir/engine_adapter",
  ]

  external_deps = [
    "hilog:libhilog",
    "image_framework:image_native",
  ]

  if (!is_arkui_x) {
    deps = [
      # "//third_party/skia_canvaskit0310:skia",
    ]
    external_deps += [ "graphic_surface:surface" ]
  }

  if (enable_text_gine) {
    defines += [ "USE_GRAPHIC_TEXT_GINE" ]
  }

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

group("2d_graphics_packages") {
  deps = [
    "$drawing_root:2d_graphics",
    "$drawing_root/drawing_ndk:native_drawing_ndk",
  ]
}
